# Lumina Learning Ltd - React Native Developer Assignment

We kindly ask you to solve the task below. By solving and submitting this assignment you provide us with insights in how you solve real-world problems. What we will be looking at are topics such as: choice of technology, structuring of code, selection of 3rd party libraries, documentation etc.

## The task

Build a React Native application that shows user's favourite movies from database.

-   It should at least show movie Title, Poster, and Plot synopsis using [OMDb API](https://www.omdbapi.com) or any other public movie API that supports IMDb ID (e.g. tt8526872).
-   It should have multiple screens. (e.g. List, Details, etc.)
-   It should also be able to add or remove list of favourite movies.
-   It should store data in local storage. (e.g. AsyncStorage, Realm, SQLite, etc.)

The data set provided in the SQL dump in this repo can be used as a starting point.
You can change original asset that you think will make the solution better.
You are allowed to use any library or framework to help you with the task.

## Bonus task

You are not required to complete all of the bonus tasks, but great if you do! We may want to discuss your approach to the bonus tasks during interview so please think of your approach to these challenges even if you do not complete them.

-   Use of Typescript
-   Have tests (unit/integration/e2e/component tests)
-   Use of Linking (e.g. movie://tt8526872)

## Expectations

Make a copy of this repo. Solve the task. Push your code to a public repo, and send us the link as a reply to our email.

Your solution should include a short readme describing your assumptions regarding the task, technology choice, your solution, how to use/test it and any final considerations such as known errors, limitation, something you struggle with, next steps, security concerns etc.

Don't worry we are not expecting this thing to be perfect.

## Note

To get an API key in [OMDb API](https://www.omdbapi.com), you will need to register with your name and email address. If you do not consent to give such details to OMDb API, please let us know and we will happily give you an API key for this assignment.
