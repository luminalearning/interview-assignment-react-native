CREATE TABLE users (
  id int(11) NOT NULL,
  firstName varchar(255) default NULL,
  lastName varchar(255) default NULL,
  favourite_movies varchar(255) default NULL
);

INSERT INTO users (id,firstName,lastName,favourite_movies)
    VALUES
        (1,'Anona','Cruz','tt0848228,tt4154756,tt2395427,tt4154796');